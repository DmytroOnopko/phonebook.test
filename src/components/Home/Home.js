import React from 'react';
import styleHome from './Home.module.scss';
import EmptyPageIcon from "../../ui/icons/emptyPage.icon";

const Home = () => {
    return (
        <div className={styleHome.content}>
            <div className={styleHome.home}>
                <EmptyPageIcon/>
                <p>Page is empty</p>
            </div>
        </div>
    )
};

export default Home;