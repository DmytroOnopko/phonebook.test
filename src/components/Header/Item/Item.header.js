import React, {memo} from 'react';
import {Link} from 'react-router-dom';
import styleItem from './Item.module.scss';

const ItemHeader = ({text, path,  active, ownRef}) =>
    <li className={styleItem.holder}
        ref={ownRef}>
        <Link className={`${styleItem.item} ${active ? styleItem['item--active'] : ''}`}
              to={path}
              >{text}</Link>
    </li>;

export default memo(ItemHeader);