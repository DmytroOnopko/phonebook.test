export const ContactPopupTypes = {
    SHOW: "CONTACT_POPUP/SHOW_CONTACT_POPUP",
    HIDE: "CONTACT_POPUP/HIDE_CONTACT_POPUP",
    SET: "CONTACT_POPUP/SET_CONTACT_POPUP"
}

export const ContactPopupAction = {
    set: item => ({
        type: ContactPopupTypes.SET,
        payload: item
    }),
    show: () => ({type: ContactPopupTypes.SHOW}),
    hide: () => ({type: ContactPopupTypes.HIDE}),
}