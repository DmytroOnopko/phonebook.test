import React from 'react';
import ContactPopup from "../components/Popup/Contact.popup";
import {connect} from "react-redux";
import {getName, getSurname, getPhone, getShow, getType} from "../selectors/contactPopup.selector";
import {ContactPopupAction} from "../actions/contactPopup.action";
import {ListItemsAction} from "../actions/listItems.action";
import {getItems} from "../selectors/listItems.selector";

const ContactPopupContainer = ({show, name, surname, phone, hide, add, update, type, items}) => {

    if (!show) return null;

    return <ContactPopup name={name}
                         surname={surname}
                         phone={phone}
                         hide={hide}
                         add={add}
                         update={update}
                         type={type}
                         show={show}
                         items={items}/>
};

export default connect(
    ({contactPopupReducer, listItemsReducer}) => ({
        name: getName(contactPopupReducer),
        surname: getSurname(contactPopupReducer),
        phone: getPhone(contactPopupReducer),
        show: getShow(contactPopupReducer),
        type: getType(contactPopupReducer),
        items: getItems(listItemsReducer)
    }),
    {
        hide: ContactPopupAction.hide,
        add: ListItemsAction.add,
        update: ListItemsAction.update
    }
)(ContactPopupContainer);