export const ListItemsTypes = {
    ADD: "LIST_ITEMS/ADD_LIST_ITEMS",
    DELETE: "LIST_ITEMS/DELETE_LIST_ITEMS",
    UPDATE: "LIST_ITEMS/UPDATE_LIST_ITEMS",
    SEARCH: "LIST_ITEMS/SEARCH_LIST_ITEMS"
}

export const ListItemsAction = {
    add: item => ({
        type: ListItemsTypes.ADD,
        payload: item
    }),
    delete: item => ({
        type: ListItemsTypes.DELETE,
        payload: item,
    }),
    update: item => ({
        type: ListItemsTypes.UPDATE,
        payload: item
    }),
    search: keyword => ({
        type: ListItemsTypes.SEARCH,
        payload: keyword
    })
}