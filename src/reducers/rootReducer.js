import {combineReducers} from "redux";
import listItemsReducer from "./listItems.reducer";
import contactPopupReducer from "./contactPopup.reducer";

export const rootReducer = combineReducers({
    listItemsReducer,
    contactPopupReducer
})