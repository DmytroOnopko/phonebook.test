import data from "../database/data.json";
import {ListItemsTypes} from "../actions/listItems.action";
import {databaseService} from "../services/database.service";

const {set, get} = databaseService;
set(data);

const INITIAL_STATE = {
    items: get(),
    keyword: "",
};

const listItemsReducer = (state = INITIAL_STATE, action) => {

    switch (action.type) {

        case ListItemsTypes.ADD:
            let copyItemsAdd = [...get()];
            copyItemsAdd.push(action.payload);
            set(copyItemsAdd);
            if (state.keyword) copyItemsAdd = filterByKeywords(state.keyword, copyItemsAdd)
            return {items: copyItemsAdd};

        case ListItemsTypes.DELETE:
            let copyItemsDelete = [...get()];
            const indexDelete = copyItemsDelete.findIndex(item => item.phone === action.payload.phone);
            copyItemsDelete.splice(indexDelete, 1);
            set(copyItemsDelete);
            if (state.keyword) copyItemsDelete = filterByKeywords(state.keyword, copyItemsDelete)
            return {items: copyItemsDelete};

        case ListItemsTypes.UPDATE:
            let copyItemsUpdate = [...get()];
            const indexUpdate = copyItemsUpdate.findIndex(item => item.phone === action.payload.phone);
            const current = copyItemsUpdate.find(item => item.phone === action.payload.phone);
            current.name = action.payload.name;
            current.phone = action.payload.phone;
            copyItemsUpdate.splice(indexUpdate, 1, current);
            set(copyItemsUpdate);
            if (state.keyword) copyItemsUpdate = filterByKeywords(state.keyword, copyItemsUpdate);
            return {items: copyItemsUpdate};

        case ListItemsTypes.SEARCH:
            let result = [...get()];
            result = filterByKeywords(action.payload, result);
            return {
                items: result,
                keyword: action.payload
            };

        default:
            return state;
    }
}

export default listItemsReducer;

const filterByKeywords = (keyword, data) => {
    const startedWord = new RegExp(`^${keyword}`, "i");
    const wordAfterSpace = new RegExp(`\\s+${keyword}`, "i");
    return data.filter(item => startedWord.test(item.name) || wordAfterSpace.test(item.name));
}