const NAME = "DATABASE";

export const databaseService = {
    set: data => localStorage.setItem(NAME, JSON.stringify(data)),
    get: () => {
        try{
            return JSON.parse(localStorage.getItem(NAME));
        } catch (e){
            return [];
        }
    }
}