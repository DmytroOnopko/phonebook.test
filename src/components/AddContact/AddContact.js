import React from 'react';
import styleContact from './AddContact.module.scss';
import PlusIcon from "../../ui/icons/plus.icon";

const AddContact = ({show}) => {

    return (
        <div className={styleContact.container}
             onClick={show}>
            <PlusIcon/>
            <p>Create contact</p>
        </div>
    )
};

export default AddContact;