import React, {useState} from 'react';
import Input from "../../ui/input/Input";
import styleContactPopup from './ContactPopup.module.scss';
import Avatar from "../Avatar/Avatar";
import Alert from "../Alert/Alert";

const ContactPopup = ({name, surname, phone, hide, add, type, update, items}) => {

    const [state, setState] = useState({
        name: name,
        surname: surname,
        phone: phone
    })

    const [alert, setAlert] = useState(false)

    const onChangeHandle = e => {
        let {value, name} = e.target;
        if (name === "phone") {
            if (!/^\d{0,10}?$/.test(value)) return;
        }
        setState({...state, ...{[e.target.name]: value.trim()}})
    }

    const handleSave = () => {
        const result = {name: `${state.name} ${state.surname}`, phone: state.phone};
        if (type === "add") {
            const contact = items.find(item => item.phone === state.phone);
            if (!contact){
                add(result);
                hide();
            } else setAlert(true);
        }else if (type === "update") {
            update(result);
            hide();
        }
    }

    return (
        <div className={styleContactPopup.container}>
            <Alert show={alert}/>
            <div className={styleContactPopup.content}>
                <Avatar name={`${state.name} ${state.surname}`}
                        setDefColor={true}/>
                <Input placeholder={"Name"}
                       value={state.name}
                       name={"name"}
                       onChange={onChangeHandle}/>
                <Input placeholder={"Surname"}
                       value={state.surname}
                       name={"surname"}
                       onChange={onChangeHandle}/>
                <Input placeholder={"Phone"}
                       value={state.phone}
                       name={"phone"}
                       onChange={onChangeHandle}/>
                <div className={styleContactPopup.buttons}>
                    <button className={`${styleContactPopup.button} ${styleContactPopup['button--close']}`}
                            onClick={hide}>Close
                    </button>
                    <button className={`${styleContactPopup.button} ${styleContactPopup['button--save']}`}
                            onClick={handleSave}>Save
                    </button>
                </div>
            </div>
        </div>
    )
};

export default ContactPopup;