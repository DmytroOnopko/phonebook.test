import React, {useEffect, useRef, useState} from 'react';
import styleHeader from './Header.module.scss';
import ItemHeader from "./Item/Item.header";
import Line from "./Line/Line";
import {useHistory} from "react-router";

const tabs = ["Home", "Contacts", "About"];

const Header = () => {

    const history = useHistory();
    const {pathname} = history.location;
    const ref = useRef();
    const [left, setLeft] = useState(15);

    useEffect(() => {
        setLeft(ref.current?.['offsetLeft'])
    }, [pathname])

    const element = item => {
        const path = `/${item}`;
        const active = pathname === path;
        return <ItemHeader key={item}
                           active={active}
                           path={path}
                           ownRef={active ? ref : null}
                           text={item}/>
    }

    return (
        <div className={styleHeader.sticky}>
            <ul className={styleHeader.header}>
                {tabs.map(element)}
                <Line left={left}/>
            </ul>
        </div>
    )
};

export default Header;