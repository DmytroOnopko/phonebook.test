import {ContactPopupTypes} from "../actions/contactPopup.action";

const INITIAL_STATE = {
    name: "",
    surname: "",
    phone: "",
    show: false,
    type: ""
};

const contactPopupReducer = (state = INITIAL_STATE, action) => {

    switch (action.type) {

        case ContactPopupTypes.SET:
            const result = action.payload.name.split(' ');
            return {
                ...state,
                ...{
                    name: result[0],
                    surname: result[1],
                    phone: action.payload.phone,
                    show: true,
                    type: "update"
                }
            }

        case ContactPopupTypes.SHOW:
            return {
                ...state,
                ...{
                    name: "",
                    surname: "",
                    phone: "",
                    show: true,
                    type: "add"
                }
            }

        case ContactPopupTypes.HIDE:
            return {
                ...state,
                ...{
                    name: "",
                    surname: "",
                    phone: "",
                    show: false,
                    type: ""
                }
            }

        default:
            return state;
    }
}

export default contactPopupReducer;