import React from 'react';
import styleInput from "./Input.module.scss";

const Input = ({value, onChange, name, placeholder}) =>
    <input className={styleInput.input}
           type="text"
           name={name}
           value={value}
           onChange={onChange}
           placeholder={placeholder}/>;

export default Input;