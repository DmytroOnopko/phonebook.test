import React, {useState, useCallback} from 'react';
import debounce from "lodash.debounce";
import styleSearch from './Search.module.scss';
import Input from "../../ui/input/Input";
const MS = 300;

const Search = ({search}) => {

    const [keyword, setKeyword] = useState("");
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const delayed = useCallback(debounce(keyword => {
        search(keyword);
    }, MS), []);

    const handleKeyword = useCallback(e => {
        setKeyword(e.target.value);
        delayed(e.target.value);
    }, [delayed]);

    return (
        <div className={styleSearch.search}
             ref={el => el}>
            <Input placeholder={"Search contacts"}
                   value={keyword}
                   name={"search"}
                   onChange={handleKeyword}/>
        </div>
    )
};

export default Search;