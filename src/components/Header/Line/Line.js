import React from 'react';
import styleLine from './Line.module.scss';

const Line = ({left}) =>
    <div className={styleLine.line} style={{marginLeft: left}}/>;

export default Line;