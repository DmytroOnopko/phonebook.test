import React from 'react';
import {isTooLightColor} from '../../services/isTooLightColor';
import colors from '../../colors/colors.json';
import {hashCode} from '../../services/hashCode';
import styleAvatar from './Avatar.module.scss';

const Avatar = ({name, setDefColor}) => {

    const index = setDefColor ? 0 : hashCode(name) % colors['length'];
    const color = {backgroundColor: setDefColor ? "#7a8fa4" : colors[index]};
    const isLight = isTooLightColor(colors[index]);
    const initials = name
        .split(' ')
        .slice(0, 2)
        .reduce((accum, item) => {
            accum.push(item[0]);
            return accum
        }, [])
        .join('');

    const style = `${styleAvatar.avatar} ${!isLight ? styleAvatar['avatar--light'] : styleAvatar['avatar--dark']}`

    return (
        <div className={style} style={color}>
            {initials}
        </div>
    )
};

export default Avatar;