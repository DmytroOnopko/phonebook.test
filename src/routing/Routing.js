import React from 'react';
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import Header from "../components/Header/Header";
import Home from "../components/Home/Home";
import About from "../components/About/About";
import ContactPopupContainer from "../containers/ContactPopup.container";
import ContactsContainer from "../containers/Contacts.container";

const Routing = () =>
    <BrowserRouter>
        <Switch>
            <Route path={"/Home"}>
                <Header/>
                <Home/>
            </Route>
            <Route path={"/Contacts"}>
                <Header/>
                <ContactsContainer/>
                <ContactPopupContainer/>
            </Route>
            <Route path={"/About"}>
                <Header/>
                <About/>
            </Route>
            <Redirect from={'/*'} to={"/Contacts"}/>
        </Switch>
    </BrowserRouter>;

export default Routing;