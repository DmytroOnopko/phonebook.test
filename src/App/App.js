import React from 'react';
import styleApp from './App.module.scss';
import Routing from "../routing/Routing";

const App = () => {

    return (
        <div className={styleApp.app}>
            <div className={styleApp.screen}>
                <Routing/>
            </div>
        </div>
    );
}

export default App;
