import React from 'react';
import styleContact from './Contact.module.scss';
import Avatar from "../Avatar/Avatar";
import DeleteIcon from "../../ui/icons/delete.icon";
import EditIcon from "../../ui/icons/edit.icon";

const Contact = ({contact, update, deleted}) => {

    const updateHandle = () => update(contact);
    const deleteHandle = () => deleted(contact);

    return (
        <li className={styleContact.contact}>
            <div className={styleContact['contact-avatar']}><Avatar name={contact.name}/></div>
            <span className={styleContact['contact-name']}>{contact.name}</span>
            <span className={styleContact['contact-phone']}>{contact.phone}</span>
            <span className={styleContact['contact-edit']}><EditIcon onClick={updateHandle}/></span>
            <span className={styleContact['contact-delete']}><DeleteIcon onClick={deleteHandle}/></span>
        </li>
    )
};

export default Contact;