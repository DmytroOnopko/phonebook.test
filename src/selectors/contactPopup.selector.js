export const getName = state => state.name;
export const getSurname = state => state.surname;
export const getPhone = state => state.phone;
export const getShow = state => state.show;
export const getType = state => state.type;