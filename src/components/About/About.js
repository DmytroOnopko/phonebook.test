import React from 'react';
import styleAbout from './About.module.scss';
import EmptyPageIcon from "../../ui/icons/emptyPage.icon";

const About = () => {
    return (
        <div className={styleAbout.content}>
            <div className={styleAbout.about}>
                <EmptyPageIcon/>
                <p>Page is empty</p>
            </div>
        </div>
    )
};

export default About;