import React from 'react';
import Contacts from "../components/Contacts/Contacts";
import {connect} from "react-redux";
import {getItems} from "../selectors/listItems.selector";
import {ListItemsAction} from "../actions/listItems.action";
import {ContactPopupAction} from "../actions/contactPopup.action";

const ContactsContainer = ({items, show, update, deleted, search}) => {

    return <Contacts items={items}
                     show={show}
                     update={update}
                     deleted={deleted}
                     search={search}/>
};

export default connect(
    ({listItemsReducer}) => ({
        items: getItems(listItemsReducer)
    }),
    {
        show: ContactPopupAction.show,
        deleted: ListItemsAction.delete,
        search: ListItemsAction.search,
        update: ContactPopupAction.set,
    }
)(ContactsContainer);