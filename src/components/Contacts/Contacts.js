import React from 'react';
import styleContacts from './Contacts.module.scss';
import Contact from "../Contact/Contact";
import AddContact from "../AddContact/AddContact";
import Search from "../Search/Search";

const Contacts = ({items, show, update, deleted, search}) => {

    const element = item => <Contact key={item.phone}
                                     contact={item}
                                     update={update}
                                     deleted={deleted}/>

    return (
        <div className={styleContacts.content}>
            <Search search={search}/>
            <AddContact show={show}/>
            <ul>{items.length > 0 ? items.map(element) : <li className={styleContacts.empty}>Page is empty</li>}</ul>
        </div>
    )
};

export default Contacts;