import React from 'react';
import styleAlert from './Alert.module.scss';

const Alert = ({show}) => {

    if (!show) return null;

    return (
        <div className={styleAlert.alert}>
            This number is exist
        </div>
    )
};

export default Alert;